import * as React from 'react'
import Layout from 'cartool/components/Layout'
import List from 'cartool/components/List'

const list: React.FunctionComponent = () => (
  <Layout title="About | Next.js + TypeScript Example">
    <List />
  </Layout>
)

export default list
