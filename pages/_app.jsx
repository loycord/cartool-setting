import App, { Container } from 'next/app'
import * as React from 'react'
import ReactDOM from 'react-dom'
import withStyles from 'styles'
import { Provider as MenuProvider } from 'contexts/menu'

class CartoolApp extends App {
  render() {
    const { Component, pageProps } = this.props
    const StyledComponent = withStyles(Component)
    return (
      <Container>
        <MenuProvider>
          <StyledComponent {...pageProps} />
        </MenuProvider>
      </Container>
    )
  }
}

export default CartoolApp
