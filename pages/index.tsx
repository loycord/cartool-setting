import * as React from 'react'
import Link from 'next/link'
import Layout from 'cartool/components/Layout'
import Login from 'cartool/modules/auth/Login'

import { Button, Form } from 'antd'

const Item = Form.Item;

function Index() {
  return (
    <Layout title="홈 | 카툴">
      <h1>Hello Next.js 👋</h1>
      <p>
        <Link href="/about">
          <a>About</a>
        </Link>
      </p>
      <Button type="primary" block>
        Primary
      </Button>
      <Button type="danger" block>
        Primary
      </Button>
      <button>Test</button>

      <Form>

      </Form>
      <Login />
    </Layout>
  )
}

export default Index
