import { createGlobalStyle } from 'styled-components'
import reset from 'styled-reset'
import typography from './typography'

export default createGlobalStyle`
  ${reset}

  * {
    box-sizing: border-box;
  }

  ${typography.toString()}
`
