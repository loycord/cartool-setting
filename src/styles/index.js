import React from 'react'
import GlobalStyle from './base'

export default Component => props => (
  <>
    <Component {...props} />
    <GlobalStyle />
  </>
)
