import styled from 'styled-components' // You need this as well
import { generateMedia } from 'styled-media-query'

const media = generateMedia({
  // desktop: '78em',
  // tablet: '60em',
  mobile: '28.125em'
})

// for example call it `Box`
// const Box = styled.div`
//   font-size: 20px;

//   ${media.lessThan("mobile")`
//     /* for screen sizes less than 60em */
//     font-size: 15px;
//   `};
// `;

export default media
