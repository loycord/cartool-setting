export const PRIMARY = '#02a1ff'
export const PRIMARY_LIGHT = '#1eacff'
export const PRIMARY_DARK = '#005ce6'
export const ERROR = '#d0021b'

export const HEADER_BACK_COLOR = '#f3f3f3'
export const FOOTER_BACK_COLOR = '#f6f3f3'

export const WHITE = '#FFF'
export const BLACK = '#000'

export const FONT = '#000'
export const FONT_LABEL = '#8c8c8c'
