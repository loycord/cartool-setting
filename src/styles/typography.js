import Typography from 'typography'
import * as colors from './colors'
import { INPUT_FONT } from './sizes'

const theme = {
  title: 'my-font',
  baseFontSize: '10px',
  baseLineHeight: 1.75,
  scaleRatio: 5 / 2,
  googleFonts: [
    {
      name: 'Noto Sans KR',
      styles: ['300', '400', '900']
    }
  ],
  headerFontFamily: ['Noto Sans KR', 'sans-serif'],
  bodyFontFamily: ['Noto Sans KR', 'sans-serif'],
  bodyColor: colors.FONT,
  headerWeight: 400,
  bodyWeight: 400,
  boldWeight: 700,
  overrideStyles: ({ adjustFontSizeTo, scale, rhythm }, option) => ({
    h1: {
      fontSize: '4.8rem'
    },
    h2: {
      fontSize: '2.8rem'
    },
    h3: {
      fontSize: '2.4rem'
    },
    h4: {
      fontSize: '2rem'
    },
    h5: {
      fontSize: '1.6rem'
    },
    h6: {
      fontSize: '1.5rem'
    },
    p: {
      fontSize: '1.4rem'
    },
    'h1, h2, h3': {
      fontWeight: 900
    },
    'h4, h5, h6, h7': {
      fontWeight: 'normal'
    },
    ul: {
      listStyle: 'disc'
    },
    'ul,ol': {
      marginLeft: 0
    },
    'h1,h2,h3,h4,h5,h6,button,input,label': {
      fontFamily: ['Noto Sans KR', 'sans-serif'].join(', ')
    },
    a: {
      // boxShadow: '0 1px 0 0 currentColor',
      color: colors.PRIMARY_DARK,
      textDecoration: 'none'
    },
    'a:hover,a:active': {
      boxShadow: 'none'
    },
    // 'mark,ins': {
    //   background: 'blue',
    //   color: 'white',
    //   padding: `${rhythm(1 / 16)} ${rhythm(1 / 8)}`,
    //   textDecoration: 'none'
    // },
    input: {
      fontSize: INPUT_FONT
    },
    button: {
      fontSize: '1.6rem'
    }
    // blockquote: {
    //   ...scale(1 / 5),
    //   color: '#999',
    //   fontStyle: 'italic',
    //   paddingLeft: rhythm(13 / 16),
    //   marginLeft: rhythm(-1),
    //   borderLeft: `${rhythm(3 / 16)} solid ${'#aaa'}`
    // },
    // 'blockquote > :last-child': {
    //   marginBottom: 0
    // },
    // // 'blockquote cite': {
    // //   ...adjustFontSizeTo(options.baseFontSize),
    // //   color: options.bodyColor,
    // //   fontWeight: options.bodyWeight,
    // // },
    // 'blockquote cite:before': {
    //   content: '"— "'
    // }
  })
}

const typography = new Typography(theme)

// Hot reload typography in development.
if (process.env.NODE_ENV !== 'production') {
  typography.injectStyles()
}

export default typography
export const rhythm = typography.rhythm
export const scale = typography.scale
