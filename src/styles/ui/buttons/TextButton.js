import styled from 'styled-components'
import * as colors from '../../colors'

const TextButton = styled.span`
  font-size: 1.4rem;
  font-weight: normal;
  color: ${colors.PRIMARY_DARK};
  margin: 1rem 0;

  cursor: pointer;
  user-select: none;

  &:hover {
    color: ${colors.PRIMARY_LIGHT};
  }

  &:active {
    color: ${colors.PRIMARY_DARK};
  }

  &:focus {
    outline: none;
  }
`

export default TextButton
