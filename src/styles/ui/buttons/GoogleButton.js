import React from 'react'
import ReactSVG from 'react-svg'
import * as colors from '../../colors'

import Button from './Button'

export default props => (
  <Button
    {...props}
    backColor={colors.GOOGLE}
    hoverColor={colors.GOOGLE_LIGHT}
    activeColor={colors.GOOGLE_DARK}
  >
    <div style={{ width: 24, height: 24 }}>
      <ReactSVG src="/static/assets/svg/img-social-login-g-24.svg" />
    </div>
  </Button>
)
