import styled from 'styled-components'
import * as colors from '../../colors'

const OutlinedButton = styled.button`
  width: 6rem;
  height: 2.4rem;
  background-color: ${colors.WHITE};
  border-color: ${colors.PRIMARY};
  border-width: 1px;
  border-style: solid;
  border-radius: 1.8rem;

  display: flex;
  justify-content: center;
  align-items: center;

  color: ${colors.PRIMARY};

  cursor: pointer;
  user-select: none;
  transition: transform 0.2s;

  &:hover {
    transform: translateY(-1px);
  }

  &:active {
    transform: translateY(0);
  }

  &:disabled {
    background-color: ${colors.DISABLED_GREY};
    cursor: not-allowed;
  }

  &:focus {
    outline: none;
  }
`

export default OutlinedButton
