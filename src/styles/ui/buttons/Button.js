import styled from 'styled-components'
import * as colors from '../../colors'

const Button = styled.button`
  width: 100%;
  height: 4.4rem;
  border: none;
  background-color: ${({ backColor }) => backColor || colors.PRIMARY};
  border-radius: 2.2rem;
  box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.2);

  display: flex;
  justify-content: center;
  align-items: center;

  color: ${colors.WHITE};

  cursor: pointer;
  user-select: none;

  &:hover {
    background-color: ${({ hoverColor }) => hoverColor || colors.PRIMARY_LIGHT};
  }

  &:active {
    background-color: ${({ activeColor }) => activeColor || colors.PRIMARY_DARK};
  }

  &:disabled {
    background-color: ${colors.DISABLED_GREY};
    cursor: not-allowed;
  }

  &:focus {
    outline: none;
  }
`

export default Button
