import React from 'react'
import ReactSVG from 'react-svg'
import * as colors from '../../colors'

import Button from './Button'

export default props => (
  <Button
    {...props}
    backColor={colors.FACEBOOK}
    hoverColor={colors.FACEBOOK_LIGHT}
    activeColor={colors.FACEBOOK_DARK}
  >
    <div style={{ width: 24, height: 24 }}>
      <ReactSVG src="/static/assets/svg/img-social-login-fb-24.svg" />
    </div>
  </Button>
)
