import styled from 'styled-components'
import * as colors from '../../colors'

export default styled.p`
  position: absolute;
  color: ${colors.ERROR};
  font-size: 11px;
  left: 1.6rem;
  bottom: -3.5rem;
`
