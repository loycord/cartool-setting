import React from 'react'
import styled from 'styled-components'

const InputContainer = styled.div`
  position: relative;
  margin: 1.5rem 0;
`

export default ({ style, children }) => <InputContainer style={style}>{children}</InputContainer>
