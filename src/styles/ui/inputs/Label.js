import styled from 'styled-components'
import * as colors from '../../colors'

export default styled.label`
  position: absolute;
  top: 50%;
  left: 1rem;
  padding: 0 8px;
  transform: translateY(-240%);
  background-color: ${colors.WHITE};

  color: ${({ isFocused, isErrored }) =>
    isErrored ? colors.ERROR : isFocused ? colors.PRIMARY : colors.FONT_LABEL};
  font-size: 1.2rem;

  transition: color 0.2s;

  z-index: 1;
`
