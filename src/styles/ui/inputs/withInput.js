import React from 'react'

export default Component =>
  class extends React.Component {
    constructor(props) {
      super(props)
      this.state = { isFocused: false, isChecked: false }
      this.handleFocus = this.handleFocus.bind(this)
      this.handleBlur = this.handleBlur.bind(this)
      this.handleCheck = this.handleCheck.bind(this)
    }

    handleFocus() {
      this.setState({ isFocused: true })
    }

    handleBlur() {
      this.setState({ isFocused: false })
    }

    handleCheck() {
      this.setState(prevState => ({ isChecked: !prevState.isChecked }))
    }

    render() {
      return (
        <Component
          {...this.props}
          inputState={{
            handleFocus: this.handleFocus,
            handleBlur: this.handleBlur,
            handleCheck: this.handleCheck,
            isFocused: this.state.isFocused,
            isChecked: this.state.isChecked
          }}
        />
      )
    }
  }
