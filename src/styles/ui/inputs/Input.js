import React from 'react'
import styled from 'styled-components'
import * as colors from '../../colors'
import * as sizes from '../../sizes'

import Label from './Label'
import InputContainer from './InputContainer'
import ErrorMessage from './ErrorMessage'

import withInput from './withInput'

const InputComponent = ({
  inputState: { handleBlur, handleFocus, isFocused },
  containerStyle,
  label,
  error,
  onFocus,
  onBlur,
  ...props
}) => (
  <InputContainer style={containerStyle}>
    <Input
      {...props}
      isErrored={error}
      onFocus={() => {
        handleFocus()
        onFocus && onFocus()
      }}
      onBlur={() => {
        handleBlur()
        onBlur && onBlur()
      }}
    />
    <Label isFocused={isFocused} isErrored={error}>
      {label}
    </Label>
    <ErrorMessage>{error}</ErrorMessage>
  </InputContainer>
)

const Input = styled.input`
  width: 100%;

  padding: ${sizes.INPUT_PADDING};
  border: 1px solid ${({ isErrored }) => (isErrored && colors.ERROR) || colors.BORDER_GREY};
  border-radius: 4px;
  transition: all 0.3s;
  &:focus {
    outline: none;
    border-color: ${({ isErrored }) => (isErrored && colors.ERROR) || colors.PRIMARY};
  }

  &::placeholder {
    color: ${colors.PLACEHOLDER_GREY};
  }
`

export default withInput(InputComponent)
