import React from 'react'
import Select from 'react-select'
import * as colors from '../../colors'
import * as sizes from '../../sizes'

import InputContainer from './InputContainer'
import Label from './Label'
import ErrorMessage from './ErrorMessage'

import withInput from './withInput'

const SelectComponent = ({
  inputState: { handleBlur, handleFocus, isFocused },
  containerStyle,
  label,
  error,
  onFocus,
  onBlur,
  ...props
}) => (
  <InputContainer style={containerStyle}>
    <Label isFocused={isFocused} isErrored={error}>
      {label}
    </Label>
    <Select
      {...props}
      styles={{
        container: styles => ({
          ...styles,
          margin: 0,
          padding: 0,
          border: `1px solid ${error ? colors.ERROR : colors.BORDER_GREY}`,
          borderRadius: '4px',
          backgroundColor: 'transparent'
        }),
        control: (styles, { data, isDisabled, isFocused, isSelected }) => ({
          ...styles,
          margin: 0,
          padding: 0,
          border: 'none'
        }),
        dropdownIndicator: styles => ({
          ...styles,
          border: 'none',
          color: colors.PRIMARY
        }),
        indicatorSeparator: styles => ({
          ...styles,
          backgroundColor: 'transparent',
          margin: 0,
          padding: 0
        }),
        input: styles => ({
          ...styles,
          margin: 0,
          width: '100%',
          padding: sizes.INPUT_PADDING,
          fontSize: sizes.INPUT_FONT,
          color: colors.FONT
        }),
        menu: styles => ({ ...styles, zIndex: '2 !important' }),
        option: (styles, { data, isDisabled, isFocused, isSelected }) => {
          return {
            ...styles,
            fontSize: '1.4rem',
            backgroundColor: isDisabled
              ? '#999'
              : isSelected
              ? colors.PRIMARY_LIGHT
              : isFocused
              ? colors.PRIMARY
              : 'transparent',
            color: isSelected || isFocused ? colors.WHITE : colors.FONT
          }
        },
        placeholder: styles => ({
          ...styles,
          margin: 0,
          padding: sizes.INPUT_PADDING,
          fontSize: sizes.INPUT_FONT,
          color: colors.PLACEHOLDER_GREY
        }),
        singleValue: (styles, { data }) => ({
          ...styles,
          padding: sizes.INPUT_PADDING,
          fontSize: sizes.INPUT_FONT,
          color: colors.FONT
        }),
        valueContainer: styles => ({ ...styles, margin: 0, padding: 0 })
      }}
      theme={theme => ({
        ...theme,
        primary: colors.PRIMARY,
        primary75: colors.PRIMARY,
        primary50: colors.PRIMARY,
        primary25: colors.PRIMARY
      })}
      onFocus={() => {
        handleFocus()
        onFocus && onFocus()
      }}
      onBlur={() => {
        handleBlur()
        onBlur && onBlur()
      }}
    />
    <ErrorMessage>{error}</ErrorMessage>
  </InputContainer>
)

export default withInput(SelectComponent)
