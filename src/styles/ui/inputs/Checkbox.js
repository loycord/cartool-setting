import React from 'react'
import styled from 'styled-components'
import * as colors from '../../colors'

import withInput from './withInput'

const CheckboxComponent = ({
  inputState: { handleCheck, isChecked },
  name,
  value,
  onChange,
  text,
  view,
  ...props
}) => (
  <Container>
    <div>
      <CheckBox
        isChecked={props.checked || isChecked}
        onClick={() => {
          handleCheck()
          onChange && onChange({ target: { name, value } })
        }}
      >
        ✓
      </CheckBox>
    </div>
    <div>
      <p style={{ marginLeft: 16, marginTop: 4 }}>
        {text}
        {view && (
          <span>
            <a href={view}>view</a>
          </span>
        )}
      </p>
    </div>
  </Container>
)

const Container = styled.div`
  display: flex;
  margin: 8px 0;

  span {
    margin-left: 4px;
  }
`

const CheckBox = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;

  width: 2.4rem;
  height: 2.4rem;
  border: 1px solid ${({ isChecked }) => (isChecked ? colors.PRIMARY : '#979797')};
  border-radius: 4px;
  cursor: pointer;

  background-color: ${({ isChecked }) => (isChecked ? colors.PRIMARY : 'transparent')};

  &:hover {
    border-color: ${colors.PRIMARY};
  }

  font-size: 2.2rem;
  color: #fff;
`

export default withInput(CheckboxComponent)
