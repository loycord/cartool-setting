import styled from 'styled-components'
import { IMG_BACKGOUND_GREY } from '../../colors'

const Thumbnail = styled.img`
  width: ${props => props.size || '3.6rem'};
  height: ${props => props.size || '3.6rem'};
  background-color: ${IMG_BACKGOUND_GREY};
`

export default Thumbnail
