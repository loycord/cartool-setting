import styled from 'styled-components'
import * as colors from '../../colors'

const CircleThumbnail = styled.img`
  width: ${props => props.size || '8rem'};
  height: ${props => props.size || '8rem'};
  background-color: ${colors.IMG_BACKGOUND_GREY};
  border-color: ${colors.WHITE};
  border-style: solid;
  border-radius: 50%;
  ${props => props.border && 'border-width: 2px;'};
  ${props => props.shadow && 'box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.5)'};
`

export default CircleThumbnail
