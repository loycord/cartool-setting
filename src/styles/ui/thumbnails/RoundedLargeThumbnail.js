import styled from 'styled-components'
import { IMG_BACKGOUND_GREY } from '../../colors'

const RoundedLargeThumbnail = styled.img`
  width: 9rem;
  height: 9.4rem;
  background-color: ${IMG_BACKGOUND_GREY};
  border-radius: 0.5rem;
  box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.5);
`

export default RoundedLargeThumbnail
