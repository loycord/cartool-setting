import styled from 'styled-components'
import { IMG_BACKGOUND_GREY } from '../../colors'

const RoundedThumbnail = styled.img`
  width: 5.4rem;
  height: 5.4rem;
  background-color: ${IMG_BACKGOUND_GREY};
  border-radius: 1rem;
`

export default RoundedThumbnail
