import styled from 'styled-components'
import { NAV_BAR_HEIGHT } from '../../sizes'
import * as colors from '../../colors'

export default styled.header`
  height: ${NAV_BAR_HEIGHT};
  background-color: ${colors.WHITE};
`
