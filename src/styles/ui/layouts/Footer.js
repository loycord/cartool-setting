import styled from 'styled-components'
import { FOOTER_HEIGHT } from '../../sizes'
import * as colors from '../../colors'

export default styled.div`
  height: ${FOOTER_HEIGHT};
  position: absolute;
  bottom: 0;
  right: 0;
  left: 0;
  background-color: ${colors.FOOTER_BACK_COLOR};

  transform: translateY(${FOOTER_HEIGHT});
`
