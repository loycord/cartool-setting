import styled from 'styled-components'
import { TAB_BAR_HEIGHT } from '../../sizes'

export default styled.div`
  position: absolute;
  top: 0;
  right: 0;
  width: 9.7rem;
  height: 100vh;
  border-top: 2px solid #d7d7d7;
`
