import styled from 'styled-components'
import { HEADER_HEIGHT } from '../../sizes'
import * as colors from '../../colors'

export default styled.header`
  height: ${HEADER_HEIGHT};
  background-color: ${colors.HEADER_BACK_COLOR};
`
