import styled from 'styled-components'
import { HEADER_HEIGHT } from '../../sizes'

export default styled.div`
  height: 100%;
  overflow: scroll;
`
