export const HEADER_HEIGHT = '6rem'
export const FOOTER_HEIGHT = '18rem'
export const NAV_BAR_HEIGHT = '4.5rem'
export const PLAYER_HEIGHT = FOOTER_HEIGHT
export const TAB_BAR_HEIGHT = FOOTER_HEIGHT

export const INPUT_FONT = '1.6rem'
export const INPUT_PADDING = '1.3rem 1.6rem'
