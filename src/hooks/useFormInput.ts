import { useState } from 'react'

function useFormInput(initialValue: string) {
  const [value, setValue] = useState(initialValue)

  function handleChange(e) {
    setValue(e.target.value)
  }

  return {
    value,
    onChange: handleChange
  }
}

export default useFormInput
