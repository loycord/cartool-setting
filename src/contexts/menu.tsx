import React, { createContext, useReducer } from 'react'

interface State {
  isHide: boolean
}

type Actions = 'on' | 'off'

interface Action {
  type: Actions
}

const initialState: State = {
  isHide: true
}

interface Reducer {
  state: State
  dispatch: React.Dispatch<any>
}

const initialReducer: Reducer = {
  state: initialState,
  dispatch: () => {}
}

const Context = createContext(initialReducer)

const reducer = (state: State, action: Action): State => {
  switch (action.type) {
    case 'off':
      return { isHide: true }
    case 'on':
      return { isHide: false }
    default:
      return state
  }
}

function Provider(props) {
  const [state, dispatch] = useReducer<State, Action>(reducer, initialState)

  return <Context.Provider value={{ state, dispatch }}>{props.children}</Context.Provider>
}

const Consumer = Context.Consumer

export { Context, Provider, Consumer }
