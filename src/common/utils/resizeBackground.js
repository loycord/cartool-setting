export const fill = (target, boxwidth, boxheight, targetwidth, targetheight) => {
    var rate, newwidth, newheight;

    rate = ( targetwidth > targetheight )? boxheight/targetheight : boxwidth/targetwidth;
    rate = ( boxheight > Math.round(targetheight*rate) )? boxheight/targetheight : ( boxwidth > targetwidth*rate )? boxwidth/targetwidth : rate;

    newwidth = Math.max(boxwidth, Math.round(targetwidth*rate));
    newheight = Math.max(boxheight, Math.round(targetheight*rate));

    return {width: newwidth,
        height: newheight,
        marginLeft: (boxwidth-newwidth)/2,
        marginTop: (boxheight-newheight)/2};
}