export default {
    url: '',

    /////////////// Admin  /////////////
    GET_ADMIN_SESSION: '/account/admin/session',
    POST_ADMIN_LOGIN: '/account/admin/login',
    POST_ACCOUNT_DELETE: '/account/user/delete',

    /////////////// User  /////////////
    /*GET*/
    GET_ACCOUNT_LIST: '/account/user/list',
    GET_ACCOUNT_LIST_COUNT: '/account/user/count',
    GET_ACCOUNT_DETAIL: '/account/user/detail',
    GET_DELETED_ACCOUNT_LIST: '/account/user/delete/list',
    GET_DELETED_ACCOUNT_LIST_COUNT: '/account/user/delete/count',
    GET_ACCOUNT_SESSION: '/account/auth/session',
    GET_USER_COUNT: '/account/user/count',
    GET_USER_LIST: '/account/user/list',
/*   GET_USER_PRIORITY: '/account/user/priority',
    GET_USER_PRIORITY_COUNT: '/account/user/priority/count',*/

    
    GET_FOLLOWER_LIST: '/account/follow/follower/list',
    GET_FOLLOWING_LIST: '/account/follow/following/list',
    GET_FOLLOWER_COUNT: '/account/follow/follower/count',
    GET_FOLLOWING_COUNT: '/account/follow/following/count',
    
    /*POST*/
    POST_ACCOUNT_NEW_PASSWORD: '/account/auth/new/password',
    POST_ACCOUNT_CHECK_EMAIL: '/account/auth/checkemail',
    POST_ACCOUNT_PASSWORDKEY: '/account/auth/passwordkey',
    POST_AUTH_LOGIN: '/account/auth/login',
    POST_AUTH_LOGOUT: '/account/auth/logout',
    POST_AUTH_CREATE: '/account/auth/signup',
    POST_ADMIN_DELETE: '/account/user/delete',
    POST_AUTH_DELETE: '/account/auth/delete',
    POST_AUTH_UPDATE: '/account/auth/update',
    POST_ADMIN_UPDATE: '/account/user/update',
    POST_FOLLOW_USER: '/account/follow',

    POST_AUTH_UPDATE_PASSWORD: '/account/auth/update/password',
    
    // POST_FOLLOW_USER: '/account/follow/following',
/*    POST_AUTH_UPDATE_PASSWORD_EMAIL: '/account/auth/update/password/email',
    POST_AUTH_UPDATE_PASSWORD_EMAIL_GENERATE: '/account/auth/update/password/email/generate',
    POST_AUTH_UPDATE_PASSWORD_OLDPASSWORD: '/account/auth/update/password/oldPassword',*/

    /////////////// Writing ////////////////////
    /*GET*/
    GET_WRITING_LIST: '/writing/list',
    GET_WRITING_COUNT: '/writing/count',
    GET_WRITING_DETAIL: '/writing/detail',
    GET_BOARD_ONE: '/board/one',
    GET_BOARD_LIST: '/board/list',
    GET_BOARD_COUNT: '/board/count',
    GET_ORDER_WRTING_LIST: '/account/user/orderwriting/list',

    /*POST*/
    POST_WRITING_CREATE: '/writing/create',
    POST_WRITING_ORDER_CREATE: '/writing/order/create',
    POST_WRITING_DELETE: '/writing/delete',
    POST_WRITING_UPDATE: '/writing/update',
    POST_WRITING_LIKE: '/writing/like',
    POST_WRITING_HIT: '/writing/hit/increase',
    POST_BOARD_CREATE: '/board/create',
    POST_BOARD_DELETE: '/board/delete',
    POST_BOARD_HIT: '/board/hit',
    POST_BOARD_LIKE: '/board/like',
    POST_BOARD_UPDATE: '/board/update',
    POST_ADMIN_BOARD_DELETE: '/board/admin/delete',


    ////////////Recommend ///////////////////////////////
    /*GET*/
    GET_RECOMMEND_ALBUM_LIST: '/recommend/album/list',
    GET_RECOMMEND_WRITER_LIST: '/recommend/user/list',
    GET_RECOMMEND_KEYWORD_LIST: '/recommend/keyword/list',
    GET_RECOMMEND_ALBUM_COUNT: '/recommend/album/count',
    GET_RECOMMEND_WRITER_COUNT: '/recommend/user/count',
    GET_RECOMMEND_KEYWORD_COUNT: '/recommend/keyword/count',

    /*POST*/
    POST_RECOMMEND_ALBUM_CREATE: '/recommend/album/create',
    POST_RECOMMEND_WRITER_CREATE: '/recommend/user/create',
    POST_RECOMMEND_KEYWORD_CREATE: '/recommend/keyword/create',
    POST_RECOMMEND_ALBUM_DELETE: '/recommend/album/delete',
    POST_RECOMMEND_WRITER_DELETE: '/recommend/user/delete',
    POST_RECOMMEND_KEYWORD_DELETE: '/recommend/keyword/delete',


/////////////// flagged ///////////////////////
    /*GET*/
    GET_FLAGGED_WRITING_LIST: '/flagged/writing/list',
    /*POST*/
    CREATE_FLAGGED_WRIITNG_LIST: '/flagged/writing/create',

/////////////// HashTag ///////////////////////
    GET_HASHTAG_ONE: '/hashtag/:hashtagId',
    GET_HASHTAG_LIST: '/hashtag/list',
    GET_HASHTAG_LIST_COUNT: '/hashtag/list/count',

    GET_HASHTAG_DETAIL: '/hashtag/detail',

    POST_HASHTAG_LIKE: '/hashtag/:hashtagId/like',
    POST_HASHTAG_DELETE: '/hashtag/delete',
    POST_HASHTAG_HIT: '/hashtag/hit/increase',
    POST_HASHTAG_UPDATE: '/hashtag/update',

    /////////////// Comment //////////////////////
    GET_COMMENT_COUNT: '/comment/list/count',
    GET_COMMENT_LIST: '/comment/list',

    POST_COMMENT_CREATE: '/comment/create',
    POST_COMMENT_DELETE: '/comment/delete',
    POST_COMMENT_LIKE: '/comment/like',
    POST_COMMENT_UPDATE: '/comment/update',


    /////////////// Notification ////////////////////////
    GET_NOTIFICATION_LIST: '/notification/list',
    GET_NOTIFICATION_COUNT: '/notification/list/count',
    POST_NOTIFICATION_UPDATE: '/notification/update',

    //////////////// FileUpload //////////////////////
    UPLOAD_IMAGE: '/media/image',
    UPLOAD_FILE: '/media/files',

    /////////////// Banner ////////////////////////
    GET_BANNER_LIST: '/banner/list',

///////////////Payment ////////////////////////////
    GET_INVOICE_LIST: '/payment/invoice/list',
    GET_INVOICE_COUNT:
        '/payment/invoice/count',
    GET_DELIVERY:
        '/payment/delivery',
    GET_DELIVERY_DEFAULT:
        '/payment/delivery/default',
    GET_ORDER_LIST:
        '/payment/order/list',
        // '/payment/order/product/list',    
    GET_PRODUCT_LIST: '/product/list',

    GET_ORDER_DETAIL:
        '/payment/order/detail',
    
    POST_PAYMENT_IAMPORT: '/payment/iamport',
    POST_CREATE_INVOICE:
        '/payment/invoice/create',
    POST_UPDATE_INVOICE:
        '/payment/invoice/update',
    POST_DELIVERY_UPDATE:
        '/payment/delivery/update',


    //////////////// Search //////////////////////
    GET_TOP_SEARCH:
        '/search/topSearchQueries',

    /*Account*/
    LOCATION_LIST : {
        LOCATION: ['강원도', '경기도', '경상남도', '경상북도', '광주광역시', '대구광역시','대전광역시','부산광역시','서울특별시','세종특별자치시','울산광역시','인천광역시','전라남도','전라북도','제주특별자치도','충청남도','충청북도'],
    },

    /*Member*/
    MEMBER_LIST: {
        TITLE: '회원 목록',
        COLUMN_NAME: ['', '번호', '필명', '가입일'],
        COLUMN_FIELD: ['', 'userId', 'name', 'createdAt'],
        COLUMN_SIZE: ['12%', '22%', '22%', '22%'],
    },
    /*Recommend*/
    RECOMMEND_ALBUM_LIST: {
        TITLE: '추천 앨범 목록',
        COLUMN_NAME: ['', '앨범아이디', '추천글', '생성일'],
        COLUMN_FIELD: ['', 'userId', 'description', 'createdAt'],
        COLUMN_SIZE: ['12%', '22%', '22%', '22%'],
    },

    RECOMMEND_KEYWORD_LIST: {
        TITLE: '추천 검색 목록',
        COLUMN_NAME: ['', '검색아이디', '키워드', '생성일'],
        COLUMN_FIELD: ['', 'keywordId', 'kewyord', 'createdAt'],
        COLUMN_SIZE: ['12%', '22%', '22%', '22%'],
    },
    RECOMMEND_USER_LIST: {
        TITLE: '추천 작가 목록',
        COLUMN_NAME: ['', '번호', '가입일'],
        COLUMN_FIELD: ['', 'userId', 'createdAt'],
        COLUMN_SIZE: ['20%', '40%', '40%'],
    },

    PAGE_MEMBER: {
        TITLE: '회원 리스트',
        COLUMN_NAME: ['index', '닉네임', '추천코드', '가입일', '최근 픽참여', '상금', '피클칩 구매', '추천수', '피클칩 반환 요청수', '환불 횟수'],
        COLUMN_FIELD: ['-', 'name', 'promotionCode', 'createdAt', 'pickedAt', 'prize', 'creditPaid', 'promotionAmount', '-', '-'],
        COLUMN_SIZE: ['10%', '10%', '10%', '10%', '10%', '10%', '10%', '10%', '10%', '10%'],
    },

    PAGE_DELETED_MEMBER: {
        TITLE: '강퇴 회원 관리',
        COLUMN_NAME: ['닉네임', '강퇴 사유', '강퇴 처리일'],
        COLUMN_FIELD: ['-', '-', '-'],
        COLUMN_SIZE: ['33.3333%', '33.3333%', '33.3333%'],
    },

    WRITING_LIST: {
        TITLE: '글 목록',
        COLUMN_NAME: ['', '번호', '제목', '내용', '필명', '게시일', '장르'],
        COLUMN_FIELD: ['', 'writingId', 'title', 'description', 'name', 'createdAt', 'genre'],
        COLUMN_SIZE: ['10%', '15%', '15%', '15%', '15%', '15%', '15%'],
    },
    //////////////Album //////////////////////////
    GET_ALBUM_LIST: '/album/list',
    GET_ALBUM_COUNT: '/album/count',
    POST_ALBUM_DELETE: '/album/delete',
    POST_ALBUM_CREATE: '/album/create',
    GET_ALBUM_DETAIL: '/album/detail',
    UPDATE_ALBUM_HIT: '/album/hit/increase',
    ALBUM_LIKE : '/album/like',
    POST_ALBUM_UPDATE: '/album/update',

    POST_ALBUM_WRITING_CREATE: '/album/writing/create',
    POST_ALBUM_WRITING_DELETE: '/album/writing/delete',
    
    //////////////board //////////////////////////
    BOARD_LIST: {
        TITLE: '게시판 리스트',
        COLUMN_NAME: ['', '번호', '내용', '작성자', '게시일'],
        COLUMN_FIELD: ['', 'boardId', '', 'category', 'writer', 'description'],
        COLUMN_SIZE: ['12%', '22%', '22%', '22%', '22%'],
    },

    HASHTAG_LIST: {
        TITLE: '해시태그 목록',
        COLUMN_NAME: ['', '번호', '해시태그명', '신규 참조일', '참조수', '조회수', '팔로잉수', '대표사진', '최초등록일'],
        COLUMN_FIELD: ['hashtagId', 'hashtag', 'updatedAt', '', 'hitAmount', '', 'imgUrl', 'createdAt'],
        COLUMN_SIZE: ['6%', '10%', '12%', '12%', '20%', '10%', '10%', '10%', '10%'],
    },


    /////////////// Notification & PickleChip ///////////
    PAGE_PICKLECHIP_LIST:
        {
            TITLE: '피클칩 관리',
            COLUMN_NAME:
                ['닉네임', '이메일', '휴대전화', '최근 접속일', '유료 피클칩수', '무료 피클칩수'],
            COLUMN_FIELD:
                ['name', 'email', 'phoneNumber', 'pickedAt', 'creditPaid', 'creditFree'],
            COLUMN_SIZE:
                ['15%', '15%', '20%', '20%', '15%', '15%'],
        },


    PAGE_PAYMENT_REQUEST_LIST:
        {
            TITLE: '출금 요청 리스트',
            COLUMN_NAME:
                ['닉네임', '추천코드', '이메일', '휴대전화', '계좌정보', '요청시간', '요청금액', '상태', '비고'],
            COLUMN_FIELD:
                ['name', 'promotionCode', 'email', 'phoneNumber', 'bankAccount', 'createdAt', 'amount', 'state', 'memo'],
            COLUMN_SIZE:
                ['10%', '10%', '10%', '10%', '10%', '10%', '10%', '10%', '20%'],
        },
    PAGE_PAYMENT_SETTING_LIST: {
        TITLE: '출금 가능금액 설정',
    },
    PAGE_PAYMENT_PAID_LIST: {
        TITLE: '지급 완료 목록',
        COLUMN_NAME:
            ['닉네임', '추천코드', '이메일', '휴대전화', '계좌정보', '요청시간', '요청금액', '상태', '비고'],
        COLUMN_FIELD:
            ['name', 'promotionCode', 'email', 'phoneNumber', 'bankAccount', 'createdAt', 'amount', 'state', 'memo'],
        COLUMN_SIZE:
            ['10%', '10%', '10%', '10%', '10%', '10%', '10%', '10%', '20%'],
    },

    SUBJECT_LIST: ['동기부여 - 내가 원하는 미래가 소설로', '대리복수 - 너에게 시원하게 복수해주겠어', '마음처방 - 그만 우울하고 싶어', '연애상담 - 남친이 이것만 바꿔줬으면', '고백&프로포즈 - 내 마음을 글로 전할게', '섹슈얼 판타지 - 이런 19금이 보고 싶어', '아이선물 - 내 아이가 이야기 속 주인공', '반려동물 - 나의 댕냥이와의 추억 만들기', '자존감 - 내가 바라는 나 그리기'],
    GENRE_LIST: ['드라마', 'SF·판타지', '로맨스' ,'19금 멜로',  '스릴러·호러', '코미디', '동화' ,'기타'],
    THEME_LIST: ['동기부여', '대리복수', '마음처방', '연애상담', '고백&프로포즈', '섹슈얼판타지', '아이선물', '반려동물', '자존감']
};
