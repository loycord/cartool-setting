import React, { useState } from 'react'
import useFormInput from 'hooks/useFormInput'
import Input from 'styles/ui/inputs/Input'

function Login() {
  const email = useFormInput('')
  const password = useFormInput('')
  const [isLoginLoading, setIsLoginLoading] = useState(false)

  function handleLoginSubmit(e: React.MouseEvent<HTMLElement>) {
    e.preventDefault()
    setIsLoginLoading(true)

    setTimeout(() => {
      setIsLoginLoading(false)
    }, 1000)
  }

  return (
    <div>
      <h1>Cartool</h1>
      <form>
        <Input id="formEmail" label="E-mail" {...email} />
        <Input label="Password" {...password} />

        <button id="loginSumit" onClick={handleLoginSubmit}>
          {isLoginLoading ? 'Logging in...' : 'Log in'}
        </button>
      </form>
    </div>
  )
}

export default Login
