import * as React from 'react'
import Link from 'next/link'
import Head from 'next/head'
import Router from 'next/router'
import NProgress from 'nprogress'
// styles
import Container from 'styles/ui/layouts/Container'
import Header from 'styles/ui/layouts/Header'
import NavBar from 'styles/ui/layouts/NavBar'
import Content from 'styles/ui/layouts/Content'
import Footer from 'styles/ui/layouts/Footer'
import SideBar from 'styles/ui/layouts/SideBar'

const NRouter = Router

NRouter.onRouteChangeStart = url => {
  console.log(`Loading: ${url}`)
  NProgress.start()
}
NRouter.onRouteChangeComplete = () => NProgress.done()
NRouter.onRouteChangeError = () => NProgress.done()

type Props = {
  title?: string
}

const Layout: React.FunctionComponent<Props> = ({
  children,
  title = 'This is the default title'
}) => (
  <div>
    <Head>
      <title>{title}</title>
      <meta charSet="utf-8" />
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />

      <link rel="stylesheet" type="text/css" href="/static/css/nprogress.css" />
    </Head>
    <Container>
      <Header style={{ backgroundColor: 'red' }}>
        <h1>header</h1>
      </Header>
      <NavBar>
        <h3>nav</h3>
      </NavBar>
      <Content style={{ backgroundColor: 'blue' }}>
        <h1>content</h1>
        <h1>content</h1>
        <h1>content</h1>
        <h1>content</h1>
   
        <h1>content</h1>
        <h1>content</h1>
        <h1>content</h1>
        <h1>content</h1>
        <h1>content</h1>
        {/* {children} */}
      </Content>
      <Footer style={{ backgroundColor: 'grey' }}>
        <h1>Footer</h1>
      </Footer>
      <SideBar style={{ backgroundColor: 'pink' }}>
        <h1>SideBar</h1>
      </SideBar>
    </Container>
  </div>
)

export default Layout
