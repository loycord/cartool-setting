const path = require('path')
const withTypescript = require('@zeit/next-typescript')
const withCSS = require('@zeit/next-css')

const cssConfig = {
  cssModules: true
}

const typescriptConfig = {
  webpack(config, options) {
    const rootDir = 'src'
    config.resolve.alias['common'] = path.join(__dirname, rootDir, 'common')
    config.resolve.alias['hocs'] = path.join(__dirname, rootDir, 'hocs')
    config.resolve.alias['hooks'] = path.join(__dirname, rootDir, 'hooks')
    config.resolve.alias['lib'] = path.join(__dirname, rootDir, 'lib')
    config.resolve.alias['contexts'] = path.join(__dirname, rootDir, 'contexts')
    config.resolve.alias['styles'] = path.join(__dirname, rootDir, 'styles')

    config.resolve.alias['admin'] = path.join(__dirname, rootDir, 'admin')
    config.resolve.alias['caro'] = path.join(__dirname, rootDir, 'caro')
    config.resolve.alias['cartool'] = path.join(__dirname, rootDir, 'cartool')

    // polyfill
    const originalEntry = config.entry
    config.entry = async () => {
      const entries = await originalEntry()

      if (entries['main.js'] && !entries['main.js'].includes('./static/js/polyfills.js')) {
        entries['main.js'].unshift('./static/js/polyfills.js')
      }

      return entries
    }
    return config
  }
}

if (typeof require !== 'undefined') {
  require.extensions['.css'] = file => {}
}

module.exports = withTypescript(
  withCSS({
    ...cssConfig,
    ...typescriptConfig
  })
)
